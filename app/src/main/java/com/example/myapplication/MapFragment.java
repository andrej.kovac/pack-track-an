package com.example.myapplication;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    MapView map;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout, container, false);
        map = view.findViewById(R.id.map);
        map.onCreate(savedInstanceState);
        map.getMapAsync(this);
        return view;
    }

    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in ..."));
        // Add a marker in Sydney and move the camera
//        googleMap.addMarker(new MarkerOptions()
//                .position(new LatLng(50.0755381, 14.4378005))
//                .title("Hello world"));
        PolylineOptions options = new PolylineOptions();
        options.color(Color.parseColor("#ffff0000"));
        options.width(5f);
        options.visible(true);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (int i = 0; i < MainActivity.array.length(); i++) {
            try {
                double lat = ((JSONObject) MainActivity.array.get(i)).getInt("lat") / 10000.0;
                double lon = ((JSONObject) MainActivity.array.get(i)).getInt("lon") / 10000.0;
                options.add(new LatLng(lat, lon));
                builder.include(new LatLng(lat, lon));
                if (lat != 0 && lon != 0) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, lon))
                            .title(((JSONObject) MainActivity.array.get(i)).getString("date") + " " + ((JSONObject) MainActivity.array.get(i)).getString("time")));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        googleMap.addPolyline(options);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 50));

    }

    @Override
    public void onResume() {
        map.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        map.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        map.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        map.onLowMemory();
        super.onLowMemory();
    }
}
