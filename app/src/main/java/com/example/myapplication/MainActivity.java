package com.example.myapplication;

import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;




public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static JSONArray array;
    static String result = "";
    public static String getResult()
    {
        try{
            result = new Task().execute(new URL("http://collector.danielzabojnik.cz/data.php/")).get();
        }
        catch(Exception e)
        {

        }
        return result;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState){
        array = new JSONArray();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        try {
            array = new DB().execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//        JSONReader js = new JSONReader();
//        Toast.makeText(getApplicationContext(), getResult(), Toast.LENGTH_SHORT).show();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        Set<Integer> topLevelDestinations = new HashSet<>();
        topLevelDestinations.add(R.id.graphFragment);
        topLevelDestinations.add(R.id.mapFragment);
        topLevelDestinations.add(R.id.settingsFragment);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(topLevelDestinations).build();

        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController((BottomNavigationView)findViewById(R.id.bottomNav), navController);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
//
//    @Override
//    public boolean onSupportNavigateUp() {
//        Navigation.findNavController(this, R.id.nav_host_fragment).navigateUp();
//        return true;
//    }

    static class Task extends AsyncTask<URL,Void, String>{

        @Override
        protected String doInBackground(URL... urls) {
            try {
                URL myUrl = new URL("http://collector.danielzabojnik.cz/data.php/");
                BufferedReader br = new BufferedReader(new InputStreamReader(myUrl.openStream()));
                String s = "";
                try {
                    s = br.readLine();
                    br.close();

                    return(s);
                }

               catch(IOException ioex)
               {

               }
            }
            catch(Exception muex)
            {

            }
            return null;
        }
    }
}
