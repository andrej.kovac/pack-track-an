package com.example.myapplication;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;
import java.util.concurrent.ExecutionException;

public class GraphFragment extends Fragment {

    static LineGraphSeries<DataPoint> series2, series;
    static DataPoint[] d = new DataPoint[10];

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (MainActivity.array == null) {

            MainActivity.array=new JSONArray();

        }
        View view = inflater.inflate(R.layout.graph_fragment, container, false);

        GraphView graph = view.findViewById(R.id.graph);

        series2 = new LineGraphSeries<>(new DataPoint[]{
                new DataPoint(0, 2),
                new DataPoint(1, 2),
                new DataPoint(2, 2),
                new DataPoint(3, 2),
                new DataPoint(4, 2),
                new DataPoint(5, 2),
                new DataPoint(6, 2),
                new DataPoint(7, 2),
                new DataPoint(8, 2),
        });

        series = new LineGraphSeries<>();
//        series.resetData(d);


        for (int i = 0; i < MainActivity.array.length(); i++){
            try {
                series.appendData(new DataPoint(i, (((JSONObject) MainActivity.array.get(i)).getInt("temp"))/10.0),true,MainActivity.array.length());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        series.setThickness(3);
        series.setDrawBackground(true);
        series.setDrawDataPoints(true);

        series2.setThickness(3);
//        series2.setBackgroundColor(getContext().getColor(R.color.lineColor));
//        series2.setDrawBackground(true);
        series2.setColor(getContext().getColor(R.color.lineColor));




        graph.addSeries(series);
        graph.addSeries(series2);

//        new Thread(this).start();

        return view;


    }

//    public static void update() {
//        while (true) {
//            Random r = new Random();
//
//            for (int I = 0; I < 9; I++) {
//                d[I] = new DataPoint(I, r.nextInt(10));
//            }
//
////            LineGraphSeries<DataPoint> series2 = new LineGraphSeries<>();
//            series.resetData(d);
//            try {
//                Thread.sleep(10000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    @Override
//    public void run() {
//        update();
//    }
}
